#!/bin/bash

#if [ $# -eq 0 ] then
#    echo "the first argument must be the path to java home"
#fi

sudo update-alternatives --install "/usr/bin/java" "java" $1"/bin/java" 1
sudo update-alternatives --install "/usr/bin/javac" "javac" $1"/bin/javac" 1
#sudo update-alternatives --install "/usr/bin/javaws.itweb" "javaws.itweb" $1"/bin/javaws.itweb" 1

sudo update-alternatives --set java $1/bin/java
sudo update-alternatives --set javac $1/bin/javac
#sudo update-alternatives --set javaws.itweb $1/bin/javaws.itweb

# set enviroment variables
echo export JAVA_HOME=$1 >> ~/.bashrc
echo export JDK_HOME=$1 >> ~/.bashrc
# echo export JRE_HOME=$1'/jre' >> ~/.bashrc
# echo export CLASSPATH=$1'/jre/bin' >> ~/.bashrc
echo export PATH=$PATH:$1'/bin' >> ~/.bashrc

source ~/.bashrc
