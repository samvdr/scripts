#!/bin/bash

sudo update-alternatives --install "/usr/bin/java" "java" "/opt/java/jdk-11.0.3/bin/java" 1
sudo update-alternatives --install "/usr/bin/javac" "javac" "/opt/java/jdk-11.0.3/bin/javac" 1
sudo update-alternatives --install "/usr/bin/javaws.itweb" "javaws.itweb" "/opt/java/jdk-11.0.3/bin/javaws.itweb" 1


sudo update-alternatives --set java /opt/java/jdk-11.0.3/bin/java
sudo update-alternatives --set javac /opt/java/jdk-11.0.3/bin/javac
sudo update-alternatives --set javaws.itweb /opt/java/jdk-11.0.3/bin/javaws.itweb
